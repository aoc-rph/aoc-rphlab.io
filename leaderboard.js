/* Copyright (C) 2020  Gloria Mundi
 * 
 *  This file is part of the AoC Leaderboard Viewer.
 *
 *  The AoC Leaderboard Viewer is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  The AoC Leaderboard Viewer is distributed in the hope that it will
 *  be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the AoC Leaderboard Viewer. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

// haha shitty code go brrr

'use strict';

{
	const SVG_XMLNS = "http://www.w3.org/2000/svg";

	const RELOAD_INTERVAL = 300000; // 5 minutes

	const USER_IGNORED = 1347711; // The aoc-viewer user

	const get_json = function(response) {
		if (response.ok) {
			return response.json();
		} else {
			throw new Error();
		}
	};

	let fetch_events = fetch('events.json').then(get_json);
	let fetch_leaderboards = fetch('leaderboards.json').then(get_json);

	/* Taken from Glasbey et al., Colour displays for categorical images.
	   http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.65.2790
	   Darker colours removed for contrast to background.
	 */
	const DISTINCT_COLORS = [
		"#ffffff", "#0000ff", "#ff0000", "#00ff00", "#ff00b6", "#ffd300",
		"#009fff", "#9a4d42", "#00ffbe", "#783fc1", "#1f9698", "#ffacfd",
		"#b1cc71", "#f1085c", "#fe8f42", "#dd00ff", "#766c95", "#02ad24",
		"#c8ff00", "#886c00", "#ffb79f", "#858567", "#a10300", "#14f9ff",
		"#00479e", "#dc5e93", "#93d4ff", "#004cff",
	];

	window.addEventListener('load', async function() {
		let now;

		const $ = document.getElementById.bind(document);

		let fetched_before = false;
		const fetch_leaderboard = function() {
			$('loading').hidden = false;
			$('error').hidden = true;
			if (fetched_before) updateGraph();
			else fetched_before = true;
			return fetch(`${lb_select['lb-leaderboard'].value}/` +
			  `${lb_select['lb-event'].value}.json`, {cache: "no-cache"})
			  .then(get_json);
		}

		let defaultEvent = null, defaultLeaderboard = null;
		const PERMA_VERSION = 1, PERMA_DATA = [
			[
				{
					type: "radio",
					form: "ordering",
					name: "ordering",
					values: ["local", "global", "stars"],
					bits: 2,
				},
				{
					type: "radio",
					form: "graph-controls",
					name: "graph",
					values: ["star-time", "star-local",
							"time-stars", "time-local"],
					bits: 2,
				},
				{
					type: "checkbox",
					id: "cumulate",
				},
				{
					type: "checkbox",
					id: "connect",
				},
				{
					type: "checkbox",
					id: "line-graphs",
				},
			],
			[
				{
					type: "radio",
					form: "ordering",
					name: "ordering",
					values: ["local", "global", "stars"],
					bits: 2,
				},
				{
					type: "radio",
					form: "graph-controls",
					name: "graph",
					values: ["star-time", "star-local",
							"time-stars", "time-local"],
					bits: 2,
				},
				{
					type: "checkbox",
					id: "cumulate",
				},
				{
					type: "checkbox",
					id: "connect",
				},
				{
					type: "checkbox",
					id: "line-graphs",
				},
			],
		];

		{
			let nojswarning = $('nojswarning');
			nojswarning.parentNode.removeChild(nojswarning);
		}

		let users = [];

		// Parse permalink
		(function() {
			let hash = window.location.hash;
			if (!hash) return;

			let data;
			try {
				data = atob(hash.substring(1));
			} catch (err) {
				if (err instanceof DOMException) {
					console.log("Someone messed with the permalink ...");
					return;
				} else {
					throw err;
				}
			}

			if (!data) return;

			let bytes = Array.from(data).map(ch => ch.charCodeAt(0));

			let version = bytes[0];
			let FORMAT = PERMA_DATA[version];
			if (FORMAT === undefined) return;

			if (version > 0) {
				let nullpos = data.indexOf('\0', 1);
				if (nullpos == -1) return;

				defaultEvent = data.substring(1, nullpos);

				defaultLeaderboard = 0;
				let i = nullpos + 1;
				while (i < bytes.length) {
					defaultLeaderboard =
					  (defaultLeaderboard << 7) | (bytes[i] & 0x7F);
					if ((bytes[i] & 0x80) == 0) break;
					i++;
				}

				data = data.substring(i);
				bytes = bytes.slice(i);
			}

			let n_bits = 0;
			for (let item of FORMAT) {
				switch (item.type) {
					case "radio": n_bits += item.bits; break;
					case "checkbox": n_bits++; break;
				}
			}

			let bits = Array(n_bits);

			let i = null;
			for (let byte of bytes) {
				if (i === null) i = 0;
				else for (let x = 0x80; x > 0 && i <= n_bits; x >>= 1) {
					bits[i++] = (byte & x) ? 1 : 0;
				}
				if (i > n_bits) break;
			}

			i = 0;
			for (let item of FORMAT) {
				switch (item.type) {
					case "radio":
						let value = 0;
						for (let j = 0; j < item.bits; j++)
						  value = (value << 1) | bits[i++];
						value = item.values[value];
						if (value === undefined) continue;
						for (let elem of
						  document.forms[item.form].elements[item.name]) {
							if (elem.value == value) {
								elem.checked = true;
								break;
							}
						}
						break;
					case "checkbox":
						$(item.id).checked = bits[i++];
						break;
					default:
						throw new Error(
						  `Unknown permalink type: ${item.type}`);
				}
			}

			let id = 0;
			for (let byte of bytes.slice(1 + Math.ceil(n_bits / 8))) {
				id = (id << 7) | (byte & 0x7F);
				if ((byte & 0x80) == 0) {
					users.push({
						id, id,
						checkbox: { checked: false }
					});
					id = 0;
				}
			}
		})();

		const updateHash = function() {
			let bits = [];

			let data = String.fromCharCode(PERMA_VERSION);
			if (PERMA_VERSION > 0) {
				data += lb_select['lb-event'].value + '\0';

				let str = "";
				let x = +lb_select['lb-leaderboard'].value;
				if (!isFinite(x)) x = 957817;
				let flag = 0x00;
				while (x) {
					str = String.fromCharCode((x & 0x7F) | flag) + str;
					x >>= 7;
					flag = 0x80;
				}

				data += str;
			}

			for (let item of PERMA_DATA[PERMA_VERSION]) {
				switch (item.type) {
					case "radio":
						let value = item.values.indexOf(
							$(item.form).elements[item.name].value
						);
						for (let x = 1 << (item.bits - 1); x > 0; x >>= 1) {
							bits.push((value & x) ? 1 : 0);
						}
						break;
					case "checkbox":
						bits.push(+$(item.id).checked);
						break;
					default:
						throw new Error(
						  `Unknown permalink type: ${item.type}`);
				}
			}

			for (let i = 0; i < bits.length; i += 8) {
				let byte = 0;
				for (let j = 0; j < 8; j++) {
					byte = (byte << 1) | bits[i+j];
				}
				data += String.fromCharCode(byte);
			}

			let uncheckedUsers = users
				.filter(user => user.stars > 0 && !user.checkbox.checked)
				.map(user => user.id);

			for (let id of uncheckedUsers) {
				let s = '';
				let highBit = 0x00;
				while (id != 0) {
					s = String.fromCharCode(highBit | (id & 0x7F)) + s;
					id >>= 7;
					highBit = 0x80;
				}
				data += s;
			}

			let hash = '#' + btoa(data).replaceAll("=", "");

			window.location.replace(hash);
		};

		const fetch_error = function() {
			$('error').hidden = false;
			updateGraph();
		};

		let interval = null;
		const startInterval = function() {
			if (interval != null) window.clearInterval(interval);
			interval = window.setInterval(async function() {
				try {
					await updateData(fetch_leaderboard());
				} catch (err) {
					window.clearInterval(interval);
					interval = null;
				}
			}, RELOAD_INTERVAL);
		}

		$('reload').onclick = function() {
			startInterval();
			updateData(fetch_leaderboard());
		};

		const lb_select = $('lb-select');

		const event_select = $('lb-event');

		let events;
		try {
			events = await fetch_events;
		} catch (err) {
			fetch_error();
			throw err;
		}

		for (let event of events) {
			let span = document.createElement('span');

			let opt = document.createElement('input');
			opt.type = 'radio';
			opt.name = 'lb-event';
			opt.id = `event-${event.id}`;
			opt.value = event.id;
			span.appendChild(opt);
			event.radio = opt;

			let label = document.createElement('label');
			label.htmlFor = opt.id;
			label.innerText = event.name;
			span.appendChild(label);

			event_select.getElementsByClassName('possible')[0]
			  .appendChild(span);
		}

		lb_select['lb-event'].value = events[events.length - 1].id;
		lb_select['lb-event'].value = defaultEvent;

		/*
		const opens =
		  Object.fromEntries(events.map(ev => [ev.id,
		    Object.fromEntries(ev.days.map(day => [day.id, day.opens]))
		  ]));
		*/
		events = Object.fromEntries(events.map(ev => [ev.id, ev]));

		const leaderboard_select = $('lb-leaderboard');

		let leaderboards;
		try {
			leaderboards = await fetch_leaderboards;
		} catch (err) {
			fetch_error();
			throw err;
		}

		for (let leaderboard of leaderboards) {
			let span = document.createElement('span');

			let radio = document.createElement('input');
			radio.type = 'radio';
			radio.name = 'lb-leaderboard';
			radio.id = `leaderboard-${leaderboard.id}`;
			radio.value = leaderboard.id;
			span.appendChild(radio);

			let label = document.createElement('label');
			label.htmlFor = radio.id;
			label.innerText = leaderboard.name;
			span.appendChild(label);

			leaderboard_select.getElementsByClassName('possible')[0]
			  .appendChild(span);
		}

		lb_select['lb-leaderboard'].value = '957817';
		lb_select['lb-leaderboard'].value = defaultLeaderboard;

		leaderboards =
		  Object.fromEntries(leaderboards.map(lb => [lb.id, lb.name]));

		const table = $('table');

		let tBody = null;

		const ORDERINGS = {
			local: {
				compare: (left, right) => right.local_score - left.local_score,
				display: user => user.local_score,
				className: 'score',
			},
			global: {
				compare: (left, right) =>
				  right.global_score - left.global_score
				    || right.local_score - left.local_score,
				display: user => user.global_score,
				className: 'score',
			},
			stars: {
				compare: (left, right) =>
				  right.stars - left.stars
				    || left.last_star_ts - right.last_star_ts,
				display: user => `${user.stars}*`,
				className: 'score gold',
			},
		};
		$('ordering').oninput = () => { updateHash(); sortUsers(); };

		const sortUsers = function() {
			if (tBody !== null) table.removeChild(tBody);
			tBody = table.createTBody();

			let ordering = ORDERINGS[$('ordering').ordering.value];

			// JavaScript's function names are so nice and descriptive!
			// Apparently it's normal to use .slice() to copy an array.
			let myusers = users.slice().sort(ordering.compare);

			let i = 0;
			let prev = null;
			for (let user of myusers) {
				let row = user.row;
				i += 1;
				if (prev === null || ordering.compare(prev, user) !== 0)
				  row.cells[0].innerText = `${i})`;
				else row.cells[0].innerText = '';
				prev = user;
				row.cells[1].innerText = ordering.display(user);
				row.cells[1].className = ordering.className;
				tBody.appendChild(row);
			}
		}

		const DOT_GRAPH = 0;
		const LINE_GRAPH = 1;
		const STEP_GRAPH = 2;
		const updateGraph = function() {
			let svg = $('svg');
			svg.setAttribute('height', svg.parentElement.clientHeight);
			svg.setAttribute('width',  svg.parentElement.clientWidth);
			while (svg.firstChild) svg.removeChild(svg.firstChild);
			for (let user of users) {
				user.svgCircles = [];
				user.svgLines = [];
			}

			let [x, y] = $('graph-controls').graph.value.split("-");

			let textElem = document.createElementNS(SVG_XMLNS, 'text');
			textElem.appendChild(document.createTextNode('1'));
			svg.appendChild(textElem);
			let bBox = textElem.getBBox();
			svg.removeChild(textElem);
			textElem = null;

			let textHeight = bBox.height;
			let vertOffset = - (bBox.y + textHeight / 2);
			let topMargin = textHeight;
			let graphHeight = svg.height.baseVal.value - 2 * textHeight;

			let calcCoords = function(getY) {
				// Unreadable code FTW
				let coords = users.map(user => user.checkbox.checked ? {
				  user: user,
				  values: metadata.days.map(day =>
				    day.day in user.completion_day_level ? [1, 2].map(star =>
				      star in user.completion_day_level[day.day] ?
				        getY(day, star,
				          user.completion_day_level[day.day][star].get_star_ts)
				        : null
				    ) : [null, null]
				  )} : null
				).filter(u => u !== null);
				return [
					Math.max(...coords.flatMap(x => x.values).flat()),
					coords,
				];
			}

			let cumulate = false;
			$('cumulate').disabled = true;
			let max, coords, lines, graphType;
			let stepGraphEnd = null;
			switch (y) {
				case "time":
					graphType = DOT_GRAPH;
					[max, coords] = calcCoords((day, star, time) =>
					                        Math.log1p(time - day.opens));
					lines = [[0, '0'], [Math.log1p(86400), '1d']];
					for (let [mul, unit, small] of
					  [[10, '1m', '10s'], [600, '1h', '10m']]) {
						lines.push([Math.log1p(mul), small]);
						for (let d of [2,3,4,5]) {
							lines.push([Math.log1p(d * mul), null]);
						}
						lines.push([Math.log1p(6 * mul), unit]);
					}
					break;
				case "local":
					$('cumulate').disabled = false;
					cumulate = $('cumulate').checked;
					graphType = cumulate ? STEP_GRAPH : DOT_GRAPH;

					coords = users.map(user => ({
						user: user,
						values: [],
					}));
					for (let day of metadata.days) {
						// In 2020, day 1 was worth no points because of an
						// outage during the puzzle unlock.
						// 2018 day 6 also doesn't count.
						if (day.opens == 1606798800
						  || day.opens == 1544072400) {
							for (let user of coords)
							  user.values.push([null, null]);
							continue;
						}
						for (let star of [1,2]) {
							let sortedUsers = (coords
							  .map((user => ({
							    user: user,
							    ts: (user.user.completion_day_level[day.day]
							      ?? {})[star]?.get_star_ts,
							  })))
							  .sort(({user: user1, ts: ts1},
							         {user: user2, ts: ts2}) =>
							    ts1 == undefined ? 1 : ts2 == undefined ? -1 :
								  ts1 - ts2)
							);
							let points = users.length;
							for (let {user, ts} of sortedUsers) {
								let day_arr;
								if (star == 1) {
									day_arr = [];
									user.values.push(day_arr);
								} else
								  day_arr = user.values[user.values.length-1];
								if (ts == undefined) {
									day_arr.push(null);
									continue;
								}
								day_arr.push(points);
								points--;
							}
						}
					}
					coords = coords.filter(({user}) => user.checkbox.checked);
					break;
				case "stars":
					cumulate = true;
					coords = calcCoords((day, star, ts) => 1)[1];
					graphType = STEP_GRAPH;
					break;
				default:
					throw new Error("Unimplemented y axis: "+y);
			}

			if (max === undefined) {
				if (cumulate) {
					max = Math.max(...coords.map(function(u) {
						let sum = 0;
						for (let v of u.values.flat()) {
							if (v != null) sum += v;
						}
						return sum;
					}));
				} else {
					max =
					  Math.max(...coords.flatMap(u => u.values).flat());
				}
			}

			if (max === -Infinity) {
				// There are 0 values. There is nothing worth rendering.
				return;
			}

			if (max == 0) {
				max = 1;
				lines = [[0, 0]];
			} else if (lines === undefined) {
				let theoreticStep = textHeight * max / graphHeight;
				let step = 10 ** Math.ceil(Math.log10(theoreticStep));
				if (step <= 1) step = 1;
				else if (theoreticStep <= 0.2 * step) step *= 0.2;
				else if (theoreticStep <= 0.5 * step) step *= 0.5;
				lines = [];
				for (let i = 0; i <= max; i += step) {
					lines.push([i, i]);
				}
			}

			lines = lines.filter(([pos, text]) => 0 <= pos && pos <= max);

			for (let item of lines) {
				let [pos, text] = item;
				if (text === null) continue;
				let textElem = document.createElementNS(SVG_XMLNS, 'text');
				textElem.appendChild(document.createTextNode(text));
				svg.appendChild(textElem);
				item[1] = textElem;
			}

			let yaxis_labels = 5 + Math.max(
			  ...lines.map(([pos, text]) => text === null ? 0 :
				                     text.getComputedTextLength()));
			let graphWidth = svg.width.baseVal.value - yaxis_labels - 3;

			for (let [pos, text] of lines) {
				let line = document.createElementNS(SVG_XMLNS, 'line');
				pos = topMargin + (max - pos) * graphHeight / max;
				line.classList.add('bg-line');
				line.setAttribute('x1', yaxis_labels);
				line.setAttribute('y1', pos);
				line.setAttribute('x2', yaxis_labels + graphWidth);
				line.setAttribute('y2', pos);
				svg.appendChild(line);

				if (text !== null) {
					text.setAttribute('y', pos + vertOffset);
					text.setAttribute('x',
					  yaxis_labels - 5 - text.getComputedTextLength());
				}
			}

			let getX, minX, maxX;
			switch (x) {
				case "star":
					if (graphType == STEP_GRAPH) graphType = LINE_GRAPH;
					getX = (day, star, time) => 2 * day + star;
					minX = 1;
					maxX = 2 * metadata.days.length;
					for (let i = 0; i < metadata.days.length; i++) {
						let day = metadata.days[i];
						let textElem =
						  document.createElementNS(SVG_XMLNS, 'text');
						textElem.appendChild(document.createTextNode(day.day));
						svg.appendChild(textElem);
						textElem.setAttribute('x', yaxis_labels
						  + graphWidth * (getX(i,1.5,null) - minX)/(maxX-minX)
						  - textElem.getBBox().width / 2);
						textElem.setAttribute('y', svg.height.baseVal.value);

						for (let star of [1,2]) {
							let line =
							  document.createElementNS(SVG_XMLNS, 'line');
							line.classList.add('bg-line');
							let x = (yaxis_labels
							  + graphWidth * (getX(i, star, null) - minX)
							               / (maxX - minX));
							line.setAttribute('x1', x);
							line.setAttribute('y1', topMargin);
							line.setAttribute('x2', x);
							line.setAttribute('y2', topMargin + graphHeight);
							svg.appendChild(line);
						}
					}
					break;
				case "time":
					getX = (day, star, time) => time;
					minX = metadata.days[0].opens;
					maxX = (2 * metadata.days[metadata.days.length-1].opens
					          - metadata.days[metadata.days.length-2].opens);
					stepGraphEnd = now;
					for (let day of metadata.days) {
						let x = (yaxis_labels
						  + graphWidth * (day.opens - minX) / (maxX - minX));

						let text = document.createElementNS(SVG_XMLNS, 'text');
						text.appendChild(document.createTextNode(day.day));
						svg.appendChild(text);
						text.setAttribute('x', x - text.getBBox().width / 2);
						text.setAttribute('y', svg.height.baseVal.value);

						let line = document.createElementNS(SVG_XMLNS, 'line');
						line.classList.add('bg-line');
						line.setAttribute('x1', x);
						line.setAttribute('y1', topMargin);
						line.setAttribute('x2', x);
						line.setAttribute('y2', topMargin + graphHeight);
						svg.appendChild(line);
					}
					break;
				default:
					throw new Error("Unimplemented x axis: "+x);
					return;
			}

			if (graphType == DOT_GRAPH) {
				let connect = $('connect');
				connect.disabled = false;
				if (connect.checked) graphType = LINE_GRAPH;
			} else $('connect').disabled = true;

			let specialLineGraph;
			if (graphType == STEP_GRAPH) {
				let lineGraph = $('line-graphs');
				lineGraph.disabled = false;
				if (lineGraph.checked) {
					graphType = LINE_GRAPH;
					specialLineGraph = true;
				}
			} else {
				$('line-graphs').disabled = true;
				specialLineGraph = false;
			}

			for (let {user, values} of coords) {
				let points = (values.flatMap((day, d) =>
				  day.flatMap((star, s) => star === null ? [] : 
				    [[getX(d, s + 1,
				    (user.completion_day_level[metadata.days[d].day] ?? {})
					  [s + 1]?.get_star_ts),
		 		    star]])
				).filter(([x, y]) => x != undefined)
				 .sort(([x1, y1], [x2, y2]) => x1 - x2));

				let path;
				if (graphType == LINE_GRAPH || graphType == STEP_GRAPH) {
					if (graphType == STEP_GRAPH || specialLineGraph)
					  path = [`M ${yaxis_labels},${topMargin + graphHeight}`];
					else path = ['M'];
				}
				let cumulator = 0;
				let title = document.createElementNS(SVG_XMLNS, 'title');
				title.appendChild(document.createTextNode(user.name));

				for (let [x, y] of points) {
					if (cumulate) y = cumulator = cumulator + y;
					let coord = {
					  x: yaxis_labels + graphWidth * (x - minX) / (maxX - minX),
					  y: topMargin + graphHeight * (max - y) / max,
					};
					let circle = document.createElementNS(SVG_XMLNS, 'circle');
					circle.setAttribute('cx', coord.x);
					circle.setAttribute('cy', coord.y);
					circle.setAttribute('r', 2);
					circle.setAttribute('fill', user.color);
					hoverUser(user, circle);

					circle.appendChild(title.cloneNode(true));
					svg.appendChild(circle);
					user.svgCircles.push(circle);

					switch (graphType) {
						case DOT_GRAPH:
							break;
						case LINE_GRAPH:
							path.push(`${coord.x},${coord.y}`);
							break;
						case STEP_GRAPH:
							path.push(`H ${coord.x}`);
							path.push(`V ${coord.y}`);
							break;
						default:
							throw new Error("Unimplemented graph type: "
							                                     +graphType);
					}
				}
				if ((graphType == STEP_GRAPH || specialLineGraph)
				  && stepGraphEnd !== null) {
					path.push('H ' + (yaxis_labels + graphWidth *
					  (stepGraphEnd - minX) / (maxX - minX)));
				}
				if (path) {
					let pathElem = document.createElementNS(SVG_XMLNS, 'path');
					pathElem.setAttribute('d', path.join(' '));
					pathElem.setAttribute('stroke', user.color);
					pathElem.setAttribute('stroke-width', 1);
					pathElem.setAttribute('fill', 'none');
					pathElem.appendChild(title); // Don't clone here
					svg.appendChild(pathElem);
					hoverUser(user, pathElem);
					user.svgLines.push(pathElem);
				}
			}
		}
		window.addEventListener('resize', updateGraph);
		$('graph-controls').oninput = () => { updateHash(); updateGraph(); };

		const updateData = async function(promise) {
			let leaderboard;
			try {
				leaderboard = await promise;
			} catch (err) {
				fetch_error();
				throw err;
			} finally {
				$('loading').hidden = true;
			}

			now = Math.floor(Date.now() / 1000);
			for (let day of metadata.days) {
				while (day.th.firstChild) {
					day.th.removeChild(day.th.firstChild);
				}

				if (now >= day.opens) {
					let a = document.createElement('a');
					a.href =
					  metadata.base_url+"day/"+day.day;
					a.target = '_blank';
					a.innerText = day.day;
					day.th.appendChild(a);
				} else {
					day.th.innerText = day.day;
				}
			}

			let checked = Object.fromEntries(users
			  .filter(user => user.stars > 0)
			  .map(user => [user.id, user.checkbox.checked]));
			users = Object.values(leaderboard.members)
			  .filter(user => user.id != USER_IGNORED);

			for (let [user, color] of users.slice()
			  .sort(ORDERINGS.local.compare)
			  .map((user, i) => [user, DISTINCT_COLORS[i]])) {
				if (user.name == null)
				  user.name = `(anonymous user #${user.id})`;
				let row = document.createElement('tr');
				row.insertCell().className = 'rank';
				row.insertCell();
				for (let day of metadata.days) {
					let cell = row.insertCell();
					if (now >= day.opens) {
						cell.innerText = '*';
						let completed =
						  user.completion_day_level[day.day];
						let className;
						if (completed === undefined ||
						  !('1' in completed)) className = 'gray';
						else if (!('2' in completed)) className = 'silver';
						else className = 'gold';
						cell.className = className;
					}
				}

				user.color = color;
				let username = row.insertCell();
				username.className = 'username';
				username.style.color = color;
				hoverUser(user, username);

				user.checkbox = document.createElement('input')
				user.checkbox.oninput = e => {
					updateHash();
					updateGraph();
					if (user.hovering) {
						if (user.checkbox.checked) focusUser(user);
						else blurUser(user);
					}
				};
				user.checkbox.type = 'checkbox';
				user.checkbox.id = `user#${user.id}`;
				user.checkbox.checked = checked[user.id] ?? (user.stars > 0);

				let label = document.createElement('label');
				label.innerText = user.name;
				label.htmlFor = user.checkbox.id;

				username.appendChild(user.checkbox);
				username.appendChild(label);

				user.row = row;
				user.hovering = false;
			};

			$('main').hidden = false;
			$('main').style.display = 'flex';
			sortUsers();
			updateGraph();
			updateHash();
		};

		const bringToFront = function(elem) {
			let parentNode = elem.parentNode;
			parentNode.removeChild(elem);
			parentNode.appendChild(elem);
		};

		const focusUser = function(user) {
			user.checkbox.parentElement.style.fontWeight = 'bold';
			for (let circle of user.svgCircles) {
				circle.setAttribute('r', 3);
				bringToFront(circle);
			}
			for (let line of user.svgLines) {
				line.setAttribute('stroke-width', 2);
				bringToFront(line);
			}
		};

		const blurUser = function(user) {
			user.checkbox.parentElement.style.fontWeight = '';
			for (let circle of user.svgCircles)
			  circle.setAttribute('r', 2);
			for (let line of user.svgLines)
			  line.setAttribute('stroke-width', 1);
		};

		const hoverUser = function(user, elem) {
			elem.onpointerenter = function() {
				user.hovering = true;
				if (user.checkbox.checked) focusUser(user);
			};
			elem.onpointerleave = function() {
				user.hovering = false;
				if (user.checkbox.checked) blurUser(user);
			};
		};

		let metadata;

		lb_select.oninput = function() {
			const event = events[this['lb-event'].value];
			metadata = {
				base_url: `https://adventofcode.com/${event.id}/`,
				days: event.days,
			};
			$('lb-event').getElementsByClassName('current')[0].innerText =
			  event.name;
			$('lb-leaderboard').getElementsByClassName('current')[0].innerText =
			  leaderboards[this['lb-leaderboard'].value];

			updateHash();

			table.deleteTHead();
			const header_row = table.createTHead().insertRow();
			header_row.insertCell();
			header_row.insertCell();
			for (let day of events[this['lb-event'].value].days) {
				const cell = document.createElement('th');
				cell.className = 'day';
				cell.innerText = day.day;
				header_row.appendChild(cell);
				day.th = cell;
			}
			$('reload').onclick();
		};
		lb_select.oninput();
	});
}

// @license-end
